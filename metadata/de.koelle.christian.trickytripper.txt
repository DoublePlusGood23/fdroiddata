Categories:Money
License:Apache2
Web Site:http://trickytripper.blogspot.de
Source Code:https://github.com/koelleChristian/trickytripper
Issue Tracker:https://github.com/koelleChristian/trickytripper/issues
Changelog:https://github.com/koelleChristian/trickytripper/blob/HEAD/app/src/main/res/raw/changelog.txt

Auto Name:Tricky Tripper
Summary:Manage travel expenses
Description:
Especially for managing shared expenses
.

Repo Type:git
Repo:https://github.com/koelleChristian/trickytripper.git

Build:1.4.2,12
    commit=c1f7c
    subdir=trickytripper-app
    srclibs=1:ActionBarSherlock@4.2.0

Build:1.5.2,15
    disable=https://github.com/koelleChristian/trickytripper/issues/2
    commit=2aff097a0e1845799b8513c4f5069c19d00eef42
    subdir=app
    gradle=yes
    rm=app/libs/*.jar
    prebuild=sed -i -e '/fileTree/acompile "com.loopj.android:android-async-http:1.4.7@aar"' build.gradle && \
        sed -i -e '/sonatype/d' ../build.gradle

Build:1.5.4,17
    disable=build failed
    commit=697c446fe1ba1edb5d2b12e9cff0dc31f486ecac
    subdir=app
    gradle=yes

Build:1.5.5,18
    commit=bfd6098207098d887f0aa1b37b075759718895e8
    subdir=app
    gradle=yes

Build:1.5.6,19
    commit=2f19e09a2d24576946c174283bb8e364e58fa735
    subdir=app
    gradle=yes
    prebuild=echo -e "android { lintOptions { disable 'ExtraTranslation' } }" >> build.gradle

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.5.6
Current Version Code:19
