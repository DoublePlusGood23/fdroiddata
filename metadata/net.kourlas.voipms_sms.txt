AntiFeatures:NonFreeNet
Categories:Phone & SMS
License:Apache2
Web Site:
Source Code:https://github.com/michaelkourlas/voipms-sms-client
Issue Tracker:https://github.com/michaelkourlas/voipms-sms-client/issues
Changelog:https://github.com/michaelkourlas/voipms-sms-client/blob/HEAD/CHANGES.md

Auto Name:VoIP.ms SMS
Summary:Send and read SMS via voip.ms
Description:
Allows you to send and receive SMS messages using your VoIP.ms account. The
application's features include:

* The ability to send, receive, and delete texts;
* Conversation-based organization (all texts to and from a particular phone number are grouped together);
* Synchronization with device contacts, including the contact's name and photo; and
* Push notifications when new texts are received using VoIP.ms SMS Server.
.

Repo Type:git
Repo:https://github.com/michaelkourlas/voipms-sms-client

Build:0.4.3-fdroid,110
    commit=9a40045058f26fea54cda25254f206fc437ea965
    subdir=voipms-sms
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest/fdroid
Current Version:0.4.3-fdroid
Current Version Code:110
