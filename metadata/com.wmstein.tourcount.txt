Categories:Science & Education
License:Apache 2.0
Web Site:
Source Code:https://github.com/wistein/TourCount
Issue Tracker:https://github.com/wistein/TourCount/issues

Summary:Species-specific counting of butterflies in nature
Description:
TourCount is an Android App that supports species-specific counting of
butterflies in nature.

The integrated database is organized according to a tour in nature with its
expected butterfly species. That means, a new (importable and prepared) basic
database instance will be used per tour.

Databases can be individually created and adapted regarding meta data and
expected butterfly species. The recorded data (counts, data and remarks) may
either be read on the smartphone or exported in SQLite- or CSV-format and
transferred to a PC for your own processing, e.g. by importing a csv-file into
MS Excel.

The app demands for storage access permit which is needed for im-/exporting the
counting data, GPS permit for location info per count and the permit to prevent
the phone from sleeping (to control the counting screen when used under Android
5.0.1 or newer).
.

Repo Type:git
Repo:https://github.com/wistein/TourCount.git

Build:2.0.7,207
    commit=0c3d71e8e492d02acdee775a7a5f3bb71e543b3a
    gradle=yes

Auto Update Mode:None
Update Check Mode:None
